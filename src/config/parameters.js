export default {
    serveurInfo: function(){
        return {
            "addressApiBack":'http://192.168.1.193:8000',
            "addressMercure":'http://localhost:3001'
        }
    },
    getPanels: function(){
        return {
            "panels":[
            {
              "id":1,
              "piece":"séjour",
              "bulbs":[
                {
                  "id":1,
                  "name":"Abat-jour"
                },
                {
                  "id":2,
                  "name":"Salon"
                },
                {
                  "id":3,
                  "name":"Suspension"
                },
                 {
                  "id":4,
                  "name":"Plafonnier"
                },
                ]
              },
               {
              "id":2,
              "piece":"cuisine",
              "bulbs":[
                {
                  "id":5,
                  "name":"Ilôt central"
                },
                {
                  "id":6,
                  "name":"Cuisinière"
                }
                ]
              },
           ]
        }
    }


  }
